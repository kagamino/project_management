package main

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/martinlehoux/tasks/common"
	"github.com/martinlehoux/tasks/db"
	"github.com/martinlehoux/tasks/task"
	"github.com/spf13/viper"
)

func main() {
	common.LoadConfig()
	var logger = common.CreateLogger()
	var timeProvider = common.RealTimeProvider{}
	var dbClient = db.CreateDatabaseClient()
	var taskStore = task.CreateSqlTaskStore(dbClient)
	var taskLoader = task.NewTasksLoader(dbClient)
	var logNewTaskHandler = task.LogNewTaskHandler{TaskStore: &taskStore, TimeProvider: &timeProvider, Logger: logger}
	var router = chi.NewRouter()
	router.Get("/status", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	})
	router.Get("/tasks/", func(w http.ResponseWriter, r *http.Request) {
		result, err := taskLoader.QueryTasks(r.Context(), task.TasksQuery{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})
	router.Post("/tasks/log_new_task", func(w http.ResponseWriter, r *http.Request) {
		var command task.LogNewTaskCommand
		err := json.NewDecoder(r.Body).Decode(&command)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		err = logNewTaskHandler.Execute(r.Context(), command)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})
	addr := viper.GetString("SERVER_ADDRESS")
	logger.Infof("listening on http://%s/", addr)
	handler := LoadTracer(router, logger)
	http.ListenAndServe(addr, handler)
}
