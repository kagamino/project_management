package burnchart

import (
	"context"
	"testing"
	"time"

	"github.com/martinlehoux/tasks/task"
	"github.com/stretchr/testify/assert"
)

func TestGetBurnchart(t *testing.T) {
	var ctx = context.Background()
	var assert = assert.New(t)

	var setUp = func() GetBurnchartHandler {
		var taskStore = task.CreateInMemTaskStore()
		return GetBurnchartHandler{
			TaskStore: &taskStore,
		}
	}

	t.Run("it should start with 0 total and 0 done", func(t *testing.T) {
		var handler = setUp()

		var query = GetBurnchartQuery{Start: time.UnixMilli(0), End: time.UnixMilli(0)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(0), total: uint(0), done: uint(0)},
		}, burnchart)
	})

	t.Run("it should add a point with a total after setting a task weight", func(t *testing.T) {
		var handler = setUp()
		var task = task.CreateTask("Hello there")
		task.SetWeight(3, time.UnixMilli(10))
		handler.TaskStore.Save(ctx, task)

		var query = GetBurnchartQuery{Start: time.UnixMilli(0), End: time.UnixMilli(10)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(0), total: uint(0), done: uint(0)},
			{time: time.UnixMilli(10), total: uint(3), done: uint(0)},
		}, burnchart)
	})

	t.Run("it should add a point with a done equaling total after marking a task as done", func(t *testing.T) {
		var handler = setUp()
		var task = task.CreateTask("Hello there")
		task.SetWeight(3, time.UnixMilli(10))
		task.MarkAsDone(time.UnixMilli((20)))
		handler.TaskStore.Save(ctx, task)

		var query = GetBurnchartQuery{Start: time.UnixMilli(0), End: time.UnixMilli(20)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(0), total: uint(0), done: uint(0)},
			{time: time.UnixMilli(10), total: uint(3), done: uint(0)},
			{time: time.UnixMilli(20), total: uint(3), done: uint(3)},
		}, burnchart)
	})

	t.Run("it should concat all events that happened before the time frame", func(t *testing.T) {
		var handler = setUp()
		var firstTask = task.CreateTask("Hello there")
		firstTask.SetWeight(3, time.UnixMilli(10))
		firstTask.MarkAsDone(time.UnixMilli((20)))
		var secondTask = task.CreateTask("Hey there")
		secondTask.SetWeight(4, time.UnixMilli(30))
		handler.TaskStore.Save(ctx, firstTask, secondTask)

		var query = GetBurnchartQuery{Start: time.UnixMilli(30), End: time.UnixMilli(30)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(30), total: uint(7), done: uint(3)},
		}, burnchart)
	})

	t.Run("it should group two events that have the same time to a single data point", func(t *testing.T) {
		var handler = setUp()
		var firstTask = task.CreateTask("Hello there")
		firstTask.SetWeight(3, time.UnixMilli(10))
		var secondTask = task.CreateTask("Hey there")
		secondTask.SetWeight(4, time.UnixMilli(10))
		handler.TaskStore.Save(ctx, firstTask, secondTask)

		var query = GetBurnchartQuery{Start: time.UnixMilli(0), End: time.UnixMilli(10)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(0), total: uint(0), done: uint(0)},
			{time: time.UnixMilli(10), total: uint(7), done: uint(0)},
		}, burnchart)
	})

	t.Run("it should have a point at the end time", func(t *testing.T) {
		var handler = setUp()
		var task = task.CreateTask("Hello there")
		task.SetWeight(3, time.UnixMilli(10))
		handler.TaskStore.Save(ctx, task)

		var query = GetBurnchartQuery{Start: time.UnixMilli(0), End: time.UnixMilli(20)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(0), total: uint(0), done: uint(0)},
			{time: time.UnixMilli(10), total: uint(3), done: uint(0)},
			{time: time.UnixMilli(20), total: uint(3), done: uint(0)},
		}, burnchart)
	})

	t.Run("it should handle a task weight changed several times", func(t *testing.T) {
		var handler = setUp()
		var task = task.CreateTask("Hello there")
		task.SetWeight(3, time.UnixMilli(10))
		task.SetWeight(5, time.UnixMilli(20))
		handler.TaskStore.Save(ctx, task)

		var query = GetBurnchartQuery{Start: time.UnixMilli(0), End: time.UnixMilli(20)}
		var burnchart, err = handler.Execute(ctx, query)

		assert.NoError(err)
		assert.Equal(Burnchart{
			{time: time.UnixMilli(0), total: uint(0), done: uint(0)},
			{time: time.UnixMilli(10), total: uint(3), done: uint(0)},
			{time: time.UnixMilli(20), total: uint(5), done: uint(0)},
		}, burnchart)
	})
}
