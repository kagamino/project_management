package burnchart

import (
	"context"
	"encoding/json"
	"time"

	"github.com/martinlehoux/tasks/common"
	"github.com/martinlehoux/tasks/task"
)

type BurnchartPoint struct {
	time  time.Time
	total uint
	done  uint
}

func (point *BurnchartPoint) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]any{
		"time":  point.time,
		"total": point.total,
		"done":  point.done,
	})
}

type Burnchart []BurnchartPoint

type GetBurnchartQuery struct {
	// Start of the time frame. Everything that happened before will be sum up at this time.
	Start time.Time
	End   time.Time
}

type GetBurnchartHandler struct {
	TaskStore task.TaskStore
}

func (handler *GetBurnchartHandler) Execute(ctx context.Context, query GetBurnchartQuery) (Burnchart, error) {
	var burnchart = make(Burnchart, 0)

	burnchart = append(burnchart, BurnchartPoint{time: query.Start, total: uint(0), done: uint(0)})

	var events, err = handler.TaskStore.Events(ctx)
	if err != nil {
		return nil, err
	}

	var taskWeights = make(map[task.TaskId]uint, 0)

	for _, event := range events {
		var time = getEventBoundedTime(event, query)

		var last = burnchart[len(burnchart)-1]
		if last.time.Equal(time) {
			burnchart[len(burnchart)-1] = updateBurnchartPoint(event, last, taskWeights)
		} else {
			var new = last
			new.time = time
			burnchart = append(burnchart, updateBurnchartPoint(event, new, taskWeights))
		}
	}

	burnchart = completeBurnchartEnd(burnchart, query)

	return burnchart, nil
}

func updateBurnchartPoint(event common.Event, base BurnchartPoint, taskWeights map[task.TaskId]uint) BurnchartPoint {
	switch e := event.(type) {
	case task.TaskWeightSetEvent:
		base.total += e.Weight - taskWeights[e.TaskId]
		taskWeights[e.TaskId] = e.Weight
	case task.TaskMarkedDoneEvent:
		base.done += e.Weight
	}
	return base
}

func getEventBoundedTime(event common.Event, query GetBurnchartQuery) time.Time {
	if event.Time().Before(query.Start) {
		return query.Start
	} else {
		return event.Time()
	}
}

func completeBurnchartEnd(burnchart Burnchart, query GetBurnchartQuery) Burnchart {
	var last = burnchart[len(burnchart)-1]
	if query.End.After(last.time) {
		return append(burnchart, BurnchartPoint{time: query.End, total: last.total, done: last.done})
	}
	return burnchart
}
