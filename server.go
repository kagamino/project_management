package main

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"go.uber.org/zap"
)

func LoadTracer(router *chi.Mux, logger *zap.SugaredLogger) http.Handler {
	var ctx = context.Background()
	logger.Info("loading tracer")
	handler := otelhttp.NewHandler(router, "")
	client := otlptracehttp.NewClient(otlptracehttp.WithInsecure())
	exporter, err := otlptrace.New(ctx, client)
	if err != nil {
		panic(err)
	}
	resource, err := resource.New(ctx, resource.WithAttributes(semconv.ServiceNameKey.String("MainService"), semconv.ServiceVersionKey.String("0.0.1")))
	if err != nil {
		panic(err)
	}
	provider := trace.NewTracerProvider(trace.WithBatcher(exporter), trace.WithResource(resource))
	otel.SetTracerProvider(provider)
	logger.Info("loaded tracer")
	return handler
}
