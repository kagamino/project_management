-- name: GetTask :one
SELECT * FROM tasks
WHERE task_id = $1 LIMIT 1;

-- name: AllTasks :many
SELECT * FROM tasks;

-- name: SaveTask :exec
INSERT INTO tasks (task_id, title, done, depends_on, weight)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (task_id) DO UPDATE SET title = $2, done = $3, depends_on = $4, weight = $5;

-- name: TaskEvents :many
SELECT * FROM task_events ORDER BY time ASC;

-- name: InsertEvent :exec
INSERT INTO task_events (type, task_id, time, payload)
VALUES ($1, $2, $3, $4);

-- name: TasksQuery :many
SELECT title, weight, done FROM tasks;
