package task

import (
	"context"
	"fmt"
	"sort"

	"github.com/martinlehoux/tasks/common"
)

type InMemTaskStore struct {
	tasks map[TaskId]Task
}

func CreateInMemTaskStore() InMemTaskStore {
	return InMemTaskStore{tasks: make(map[TaskId]Task)}
}

func (store *InMemTaskStore) All(ctx context.Context) (map[TaskId]Task, error) {
	var tasks = make(map[TaskId]Task)
	for _, task := range store.tasks {
		task.events = make([]common.Event, 0)
		tasks[task.id] = task
	}
	return tasks, nil
}

func (store *InMemTaskStore) Save(ctx context.Context, tasks ...Task) error {
	for _, task := range tasks {
		store.tasks[task.id] = task
	}
	return nil
}

func (store *InMemTaskStore) Get(ctx context.Context, taskId TaskId) (Task, error) {
	var task, exists = store.tasks[taskId]
	if !exists {
		return Task{}, fmt.Errorf("task not found")
	}
	task.events = make([]common.Event, 0)
	return task, nil
}

func (store *InMemTaskStore) Events(ctx context.Context) ([]common.Event, error) {
	var events = make([]common.Event, 0)
	for _, task := range store.tasks {
		events = append(events, task.events...)
	}
	sort.Slice(events, func(i, j int) bool {
		return events[i].Time().Before(events[j].Time())
	})
	return events, nil
}

func (store *InMemTaskStore) Reset() {
	store.tasks = make(map[TaskId]Task)
}
