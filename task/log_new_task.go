package task

import (
	"context"

	"github.com/martinlehoux/tasks/common"
	"go.uber.org/zap"
)

type LogNewTaskCommand struct {
	Title  string `json:"title"`
	Weight uint   `json:"weight"`
}

type LogNewTaskHandler struct {
	TaskStore    TaskStore
	TimeProvider common.TimeProvider
	Logger       *zap.SugaredLogger
}

func (handler *LogNewTaskHandler) Execute(ctx context.Context, command LogNewTaskCommand) error {
	var logger = handler.Logger.With("command", "LogNewTaskCommand")
	logger.Info("logging new task")

	var task = CreateTask(command.Title)
	logger = logger.With("taskId", task.id)

	if command.Weight > 0 {
		err := task.SetWeight(command.Weight, handler.TimeProvider.Now())
		if err != nil {
			logger.Errorw("failed to set task weight", "error", err)
			return err
		}
	}

	err := handler.TaskStore.Save(ctx, task)
	if err != nil {
		logger.Errorw("failed to save task", "error", err)
		return err
	}

	return nil
}
