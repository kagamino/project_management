package task

import (
	"context"
	"testing"
	"time"

	"github.com/martinlehoux/tasks/common"
	"github.com/martinlehoux/tasks/db"
	"github.com/stretchr/testify/assert"
)

func TestInMemTaskStore(t *testing.T) {
	var inMemTaskStore = CreateInMemTaskStore()
	var setUp = func() {
		inMemTaskStore.Reset()
	}
	TaskStoreTestSuite(t, &inMemTaskStore, setUp)
}

func TestSqlTaskStore(t *testing.T) {
	common.LoadConfig()
	var sqlTaskStore = CreateSqlTaskStore(db.CreateDatabaseClient())
	var setUp = func() {
		sqlTaskStore.client.Exec(`CREATE TABLE IF NOT EXISTS tasks (
			task_id BIGINT PRIMARY KEY,
			title TEXT NOT NULL,
			done BOOLEAN NOT NULL,
			depends_on BIGINT[] NOT NULL,
			weight BIGINT NOT NULL
		)`)
		sqlTaskStore.client.Exec(`CREATE TABLE IF NOT EXISTS task_events (
			id BIGSERIAL PRIMARY KEY,
			type CHAR(255) NOT NULL,
			task_id BIGINT NOT NULL,
			time TIMESTAMP NOT NULL,
			payload JSONB NOT NULL
		)`)
		sqlTaskStore.client.Exec("TRUNCATE TABLE tasks")
		sqlTaskStore.client.Exec("TRUNCATE TABLE task_events")
	}
	TaskStoreTestSuite(t, &sqlTaskStore, setUp)
}

func TaskStoreTestSuite(t *testing.T, store TaskStore, setUp func()) {
	var assert = assert.New(t)
	var ctx = context.Background()

	t.Run("it should save and get a Task without events", func(t *testing.T) {
		setUp()
		var task = CreateTask("Hello there")
		task.SetWeight(3, time.Now())
		task.MarkAsDone(time.Now())

		var err = store.Save(ctx, task)

		assert.NoError(err)

		savedTask, err := store.Get(ctx, task.id)

		assert.NoError(err)
		task.events = make([]common.Event, 0)
		assert.Equal(task, savedTask)
	})

	t.Run("it should save a Task multiple times", func(t *testing.T) {
		setUp()
		var task = CreateTask("Hello there")
		task.SetWeight(3, time.Now())

		store.Save(ctx, task)

		task.MarkAsDone(time.Now())

		store.Save(ctx, task)

		savedTask, err := store.Get(ctx, task.id)

		assert.NoError(err)
		task.events = make([]common.Event, 0)
		assert.Equal(task, savedTask)
	})

	t.Run("it should get all events sorted", func(t *testing.T) {
		setUp()
		var firstTask = CreateTask("Hello world")
		firstTask.SetWeight(3, time.UnixMilli(10))
		var secondTask = CreateTask("Me too")
		secondTask.SetWeight(4, time.UnixMilli(20))
		firstTask.MarkAsDone(time.UnixMilli(30))
		store.Save(ctx, firstTask, secondTask)

		var events, err = store.Events(ctx)

		assert.NoError(err)
		assert.Len(events, 3)
		assert.Equal(firstTask.id, events[0].(TaskWeightSetEvent).TaskId)
		assert.Equal(secondTask.id, events[1].(TaskWeightSetEvent).TaskId)
		assert.Equal(firstTask.id, events[2].(TaskMarkedDoneEvent).TaskId)
	})
}
