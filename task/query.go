package task

import (
	"context"
	"database/sql"

	"github.com/martinlehoux/tasks/db"
)

type TaskDto struct {
	Title  string `json:"title"`
	Weight uint   `json:"weight"`
	Done   bool   `json:"done"`
}

type TasksQuery struct {
}

type TasksLoader struct {
	client  *sql.DB
	queries db.Queries
}

func NewTasksLoader(client *sql.DB) TasksLoader {
	var queries = db.New(client)
	return TasksLoader{
		client:  client,
		queries: *queries,
	}
}

func (loader *TasksLoader) QueryTasks(ctx context.Context, query TasksQuery) ([]TaskDto, error) {
	result, err := loader.queries.TasksQuery(ctx)
	if err != nil {
		return []TaskDto{}, err
	}
	tasks := make([]TaskDto, len(result))
	for _, row := range result {
		tasks = append(tasks, rowToTaskDto(row))
	}
	return tasks, nil
}

func rowToTaskDto(row db.TasksQueryRow) TaskDto {
	return TaskDto{
		Title:  row.Title,
		Weight: uint(row.Weight),
		Done:   row.Done,
	}
}
