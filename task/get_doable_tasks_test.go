package task

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetDoableTasks(t *testing.T) {
	var assert = assert.New(t)
	var ctx = context.Background()

	var setUp = func() GetDoableTasksHandler {
		var taskStore = CreateInMemTaskStore()
		return GetDoableTasksHandler{
			taskStore: &taskStore,
		}
	}

	t.Run("it should return the task whe there is a single task", func(t *testing.T) {
		var handler = setUp()
		var task = CreateTask("Hello there")
		handler.taskStore.Save(ctx, task)

		var tasks, err = handler.Execute(ctx)

		assert.NoError(err)
		assert.Len(tasks, 1)
		assert.Equal(task.id, tasks[0].id)
	})

	t.Run("it should not return a task that is done", func(t *testing.T) {
		var handler = setUp()
		var task = CreateTask("Already done")
		task.MarkAsDone(time.Now())
		handler.taskStore.Save(ctx, task)

		var tasks, err = handler.Execute(ctx)

		assert.NoError(err)
		assert.Len(tasks, 0)
	})

	t.Run("it not return a task that depends on another task", func(t *testing.T) {
		var handler = setUp()
		var firstTask = CreateTask("First task")
		var secondTask = CreateTask("Second task")
		secondTask.AddDependOn(firstTask)
		handler.taskStore.Save(ctx, firstTask, secondTask)

		var tasks, err = handler.Execute(ctx)

		assert.NoError(err)
		assert.Len(tasks, 1)
		assert.Equal(firstTask.id, tasks[0].id)
	})

	t.Run("it should return a task that depends on a done task", func(t *testing.T) {
		var handler = setUp()
		var firstTask = CreateTask("First task")
		firstTask.MarkAsDone(time.Now())
		var secondTask = CreateTask("Second task")
		secondTask.AddDependOn(firstTask)
		handler.taskStore.Save(ctx, firstTask, secondTask)

		var tasks, err = handler.Execute(ctx)

		assert.NoError(err)
		assert.Len(tasks, 1)
		assert.Equal(secondTask.id, tasks[0].id)
	})

	t.Run("it should not return any task in a loop schema", func(t *testing.T) {
		var handler = setUp()
		var firstTask = CreateTask("First")
		var secondTask = CreateTask("Second")
		var thirdTask = CreateTask("Third")
		secondTask.AddDependOn(firstTask)
		thirdTask.AddDependOn(secondTask)
		firstTask.AddDependOn(thirdTask)
		handler.taskStore.Save(ctx, firstTask, secondTask, thirdTask)

		var tasks, err = handler.Execute(ctx)

		assert.NoError(err)
		assert.Len(tasks, 0)
	})

	t.Run("it should not return a task that depends on a done task and a not done task", func(t *testing.T) {
		var handler = setUp()
		var doneTask = CreateTask("Done")
		doneTask.MarkAsDone(time.Now())
		var notDoneTask = CreateTask("Not done")
		var task = CreateTask("Task")
		task.AddDependOn(notDoneTask)
		task.AddDependOn(doneTask)
		handler.taskStore.Save(ctx, doneTask, notDoneTask, task)

		var tasks, err = handler.Execute(ctx)

		assert.NoError(err)
		assert.Len(tasks, 1)
	})

}
