package task

import (
	"context"

	"github.com/martinlehoux/tasks/common"
	"go.uber.org/zap"
)

type MarkAsDoneCommand struct {
	TaskId TaskId `json:"task_id"`
}

type MarkAsDoneHandler struct {
	TaskStore    TaskStore
	TimeProvider common.TimeProvider
	Logger       *zap.SugaredLogger
}

func (handler *MarkAsDoneHandler) Execute(ctx context.Context, command MarkAsDoneCommand) error {
	var logger = handler.Logger.With("command", "MarkAsDoneCommand").With("taskId", command.TaskId)
	logger.Info("marking task as done")

	var task, err = handler.TaskStore.Get(ctx, command.TaskId)
	if err != nil {
		logger.Errorw("failed to retrieve task", "error", err)
		return err
	}

	task.MarkAsDone(handler.TimeProvider.Now())

	err = handler.TaskStore.Save(ctx, task)
	if err != nil {
		logger.Errorw("failed to save task", "error", err)
		return err
	}

	logger.Info("task marked as done")
	return nil
}
