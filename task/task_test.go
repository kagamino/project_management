package task

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTaskIsDone(t *testing.T) {
	var assert = assert.New(t)

	t.Run("it should not be done by default", func(t *testing.T) {
		var task = CreateTask("Hello there")

		assert.False(task.IsDone())
	})

	t.Run("it should be done after marked as done", func(t *testing.T) {
		var task = CreateTask("Hello there")
		task.MarkAsDone(time.Now())

		assert.True(task.IsDone())
	})
}
