package task

import (
	"context"
	"database/sql"
	"encoding/json"
	"reflect"
	"strings"

	"github.com/martinlehoux/tasks/common"
	"github.com/martinlehoux/tasks/db"
)

type SqlTaskStore struct {
	client  *sql.DB
	queries db.Queries
}

func CreateSqlTaskStore(client *sql.DB) SqlTaskStore {
	var queries = db.New(client)
	return SqlTaskStore{
		client:  client,
		queries: *queries,
	}
}

func (store *SqlTaskStore) Get(ctx context.Context, taskId TaskId) (Task, error) {
	var err error

	row, err := store.queries.GetTask(ctx, int64(taskId))
	if err != nil {
		return Task{}, err
	}

	return rowToTask(row), nil
}

func (store *SqlTaskStore) All(ctx context.Context) (map[TaskId]Task, error) {
	var tasks = make(map[TaskId]Task, 0)

	rows, err := store.queries.AllTasks(ctx)
	if err != nil {
		return nil, err
	}

	for _, row := range rows {
		tasks[TaskId(row.TaskID)] = rowToTask(row)
	}

	return tasks, nil
}

func (store *SqlTaskStore) Save(ctx context.Context, tasks ...Task) error {
	tx, err := store.client.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	var rollback = func(err error) error {
		tx.Rollback()
		return err
	}

	for _, task := range tasks {
		var dependsOn = make([]int64, 0)
		for _, id := range task.dependsOn {
			dependsOn = append(dependsOn, int64(id))
		}
		err := store.queries.WithTx(tx).SaveTask(ctx, db.SaveTaskParams{
			TaskID:    int64(task.id),
			Title:     task.title,
			Done:      task.done,
			DependsOn: dependsOn,
			Weight:    int64(task.weight),
		})
		if err != nil {
			return rollback(err)
		}
		for _, event := range task.events {
			payload, err := json.Marshal(event)
			if err != nil {
				rollback(err)
			}
			err = store.queries.WithTx(tx).InsertEvent(ctx, db.InsertEventParams{
				Type:    reflect.TypeOf(event).Name(),
				Time:    event.Time(),
				TaskID:  int64(task.id),
				Payload: payload,
			})
			if err != nil {
				rollback(err)
			}
		}
	}

	return tx.Commit()
}

func (store *SqlTaskStore) Events(ctx context.Context) ([]common.Event, error) {
	var events = make([]common.Event, 0)

	rows, err := store.queries.TaskEvents(ctx)
	if err != nil {
		return nil, err
	}
	for _, row := range rows {
		events = append(events, rowToEvent(row))
	}
	return events, nil
}

func rowToTask(row db.Task) Task {
	var taskId = TaskId(row.TaskID)
	var dependsOn = make([]TaskId, len(row.DependsOn))
	for _, id := range row.DependsOn {
		dependsOn = append(dependsOn, TaskId(id))
	}
	return Task{
		id:        taskId,
		events:    make([]common.Event, 0),
		title:     row.Title,
		done:      row.Done,
		weight:    uint(row.Weight),
		dependsOn: dependsOn,
	}
}

func rowToEvent(row db.TaskEvent) common.Event {
	var event common.Event

	switch strings.ReplaceAll(row.Type, " ", "") {
	case "TaskWeightSetEvent":
		var payload struct {
			Weight uint
		}
		json.Unmarshal(row.Payload, &payload)
		event = TaskWeightSetEvent{
			time:   row.Time,
			TaskId: TaskId(row.TaskID),
			Weight: payload.Weight,
		}
	case "TaskMarkedDoneEvent":
		var payload struct {
			Weight uint
		}
		json.Unmarshal(row.Payload, &payload)
		event = TaskMarkedDoneEvent{
			time:   row.Time,
			TaskId: TaskId(row.TaskID),
			Weight: payload.Weight,
		}
	}
	return event
}
