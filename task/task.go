package task

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/martinlehoux/tasks/common"
)

type TaskId uint

type Task struct {
	id        TaskId
	events    []common.Event
	title     string
	done      bool
	dependsOn []TaskId
	weight    uint
}

type TaskWeightSetEvent struct {
	TaskId TaskId
	Weight uint
	time   time.Time
}

func (event TaskWeightSetEvent) Time() time.Time {
	return event.time
}

type TaskMarkedDoneEvent struct {
	TaskId TaskId
	Weight uint // TODO: Remove unnecessary
	time   time.Time
}

func (event TaskMarkedDoneEvent) Time() time.Time {
	return event.time
}

func CreateTask(title string) Task {
	return Task{
		id:        TaskId(rand.Uint32()),
		events:    make([]common.Event, 0),
		title:     title,
		done:      false,
		dependsOn: make([]TaskId, 0),
		weight:    0,
	}
}

func (task *Task) IsDone() bool {
	return task.done
}

func (task *Task) SetWeight(weight uint, time time.Time) error {
	if task.done {
		return fmt.Errorf("task done can't set weight")
	}
	task.weight = weight
	task.events = append(task.events, TaskWeightSetEvent{TaskId: task.id, Weight: weight, time: time})
	return nil
}

func (task *Task) MarkAsDone(time time.Time) {
	if !task.done {
		task.done = true
		task.events = append(task.events, TaskMarkedDoneEvent{TaskId: task.id, Weight: task.weight, time: time})
	}
}

func (task *Task) AddDependOn(dependsOn Task) {
	task.dependsOn = append(task.dependsOn, dependsOn.id)
}

func Main() string {
	return "Hello world"
}
