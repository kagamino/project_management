package task

import (
	"context"
	"fmt"
)

type GetDoableTasksHandler struct {
	taskStore TaskStore
}

func (handler GetDoableTasksHandler) Execute(ctx context.Context) ([]Task, error) {
	var allTasks, err = handler.taskStore.All(ctx)
	if err != nil {
		return nil, err
	}
	var tasks = make([]Task, 0)

	for _, task := range allTasks {
		if task.done {
			continue
		}
		var isBlocked = false
		for _, dependsOnId := range task.dependsOn {
			var dependsOn, exists = allTasks[dependsOnId]
			if !exists {
				return tasks, fmt.Errorf("missing task")
			}
			if !dependsOn.done {
				isBlocked = true
			}
		}
		if !isBlocked {
			tasks = append(tasks, task)
		}
	}

	return tasks, nil
}
