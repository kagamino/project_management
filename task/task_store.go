package task

import (
	"context"

	"github.com/martinlehoux/tasks/common"
)

type TaskStore interface {
	Save(ctx context.Context, tasks ...Task) error
	All(ctx context.Context) (map[TaskId]Task, error)
	Get(ctx context.Context, taskId TaskId) (Task, error)
	// Events are sorted by time
	Events(ctx context.Context) ([]common.Event, error)
}
