-- migrate:up
CREATE TABLE tasks (
  task_id BIGINT PRIMARY KEY,
  title TEXT NOT NULL,
  done BOOLEAN NOT NULL,
  depends_on BIGINT[] NOT NULL,
  weight BIGINT NOT NULL
);
CREATE TABLE task_events (
  id BIGSERIAL PRIMARY KEY,
  type CHAR(255) NOT NULL,
  task_id BIGINT NOT NULL,
  time TIMESTAMP NOT NULL,
  payload JSONB NOT NULL
);

-- migrate:down
DROP TABLE tasks;
DROP TABLE task_events;