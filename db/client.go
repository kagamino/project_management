package db

import (
	"database/sql"

	"github.com/XSAM/otelsql"
	"github.com/spf13/viper"
)

func CreateDatabaseClient() *sql.DB {
	var url = viper.GetString("DATABASE_URL")
	var client, err = otelsql.Open("postgres", url)
	if err != nil {
		panic(err)
	}
	err = client.Ping()
	if err != nil {
		panic(err)
	}
	return client
}
