// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: query.sql

package db

import (
	"context"
	"encoding/json"
	"time"

	"github.com/lib/pq"
)

const allTasks = `-- name: AllTasks :many
SELECT task_id, title, done, depends_on, weight FROM tasks
`

func (q *Queries) AllTasks(ctx context.Context) ([]Task, error) {
	rows, err := q.db.QueryContext(ctx, allTasks)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Task
	for rows.Next() {
		var i Task
		if err := rows.Scan(
			&i.TaskID,
			&i.Title,
			&i.Done,
			pq.Array(&i.DependsOn),
			&i.Weight,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const getTask = `-- name: GetTask :one
SELECT task_id, title, done, depends_on, weight FROM tasks
WHERE task_id = $1 LIMIT 1
`

func (q *Queries) GetTask(ctx context.Context, taskID int64) (Task, error) {
	row := q.db.QueryRowContext(ctx, getTask, taskID)
	var i Task
	err := row.Scan(
		&i.TaskID,
		&i.Title,
		&i.Done,
		pq.Array(&i.DependsOn),
		&i.Weight,
	)
	return i, err
}

const insertEvent = `-- name: InsertEvent :exec
INSERT INTO task_events (type, task_id, time, payload)
VALUES ($1, $2, $3, $4)
`

type InsertEventParams struct {
	Type    string
	TaskID  int64
	Time    time.Time
	Payload json.RawMessage
}

func (q *Queries) InsertEvent(ctx context.Context, arg InsertEventParams) error {
	_, err := q.db.ExecContext(ctx, insertEvent,
		arg.Type,
		arg.TaskID,
		arg.Time,
		arg.Payload,
	)
	return err
}

const saveTask = `-- name: SaveTask :exec
INSERT INTO tasks (task_id, title, done, depends_on, weight)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (task_id) DO UPDATE SET title = $2, done = $3, depends_on = $4, weight = $5
`

type SaveTaskParams struct {
	TaskID    int64
	Title     string
	Done      bool
	DependsOn []int64
	Weight    int64
}

func (q *Queries) SaveTask(ctx context.Context, arg SaveTaskParams) error {
	_, err := q.db.ExecContext(ctx, saveTask,
		arg.TaskID,
		arg.Title,
		arg.Done,
		pq.Array(arg.DependsOn),
		arg.Weight,
	)
	return err
}

const taskEvents = `-- name: TaskEvents :many
SELECT id, type, task_id, time, payload FROM task_events ORDER BY time ASC
`

func (q *Queries) TaskEvents(ctx context.Context) ([]TaskEvent, error) {
	rows, err := q.db.QueryContext(ctx, taskEvents)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []TaskEvent
	for rows.Next() {
		var i TaskEvent
		if err := rows.Scan(
			&i.ID,
			&i.Type,
			&i.TaskID,
			&i.Time,
			&i.Payload,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const tasksQuery = `-- name: TasksQuery :many
SELECT title, weight, done FROM tasks
`

type TasksQueryRow struct {
	Title  string
	Weight int64
	Done   bool
}

func (q *Queries) TasksQuery(ctx context.Context) ([]TasksQueryRow, error) {
	rows, err := q.db.QueryContext(ctx, tasksQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []TasksQueryRow
	for rows.Next() {
		var i TasksQueryRow
		if err := rows.Scan(&i.Title, &i.Weight, &i.Done); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
