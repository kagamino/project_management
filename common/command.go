package common

import (
	"context"
	"fmt"
	"reflect"
)

type CommandBus struct {
	handlers map[string]CommandHandler[any]
}

func NewCommandBus() CommandBus {
	return CommandBus{handlers: make(map[string]CommandHandler[any])}
}

func (bus *CommandBus) Register(command any, handler CommandHandler[any]) {
	bus.handlers[reflect.TypeOf(command).Name()] = handler
}

func (bus *CommandBus) Execute(ctx context.Context, command any) error {
	var name = reflect.TypeOf(command).Name()
	var handler, ok = bus.handlers[name]
	if !ok {
		return fmt.Errorf("no handler for command %s", name)
	}
	return handler.Execute(ctx, command)
}

type CommandHandler[C any] interface {
	Execute(ctx context.Context, command C) error
}
