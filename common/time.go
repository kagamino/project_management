package common

import "time"

type TimeProvider interface {
	Now() time.Time
}

type FakeTimeProvider struct {
	now time.Time
}

func CreateFakeTimeProvider() FakeTimeProvider {
	return FakeTimeProvider{now: time.UnixMilli(0)}
}

func (provider *FakeTimeProvider) Now() time.Time {
	return provider.now
}

func (provider *FakeTimeProvider) SetNow(time time.Time) {
	provider.now = time
}

func (provider *FakeTimeProvider) Reset() {
	provider.now = time.UnixMilli(0)
}

type RealTimeProvider struct{}

func (provider *RealTimeProvider) Now() time.Time {
	return time.Now()
}
