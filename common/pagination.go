package common

type Paginated[C any] struct {
	Items []C  `json:"items"`
	Total uint `json:"total"`
}

type Pagination struct {
	Take uint `json:"take"`
	Skip uint `json:"skip"`
}
